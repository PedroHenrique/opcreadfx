/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opcreadfx;

import Jama.Matrix;
import classes.OPC;
import classes.PushNoti;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Label;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import eu.hansolo.tilesfx.Tile;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafish.clients.opc.JOpc;
import javafish.clients.opc.component.OpcGroup;
import javafish.clients.opc.component.OpcItem;
import javafish.clients.opc.exception.ComponentNotFoundException;
import javafish.clients.opc.exception.ConnectivityException;
import javafish.clients.opc.exception.SynchReadException;
import javafish.clients.opc.exception.SynchWriteException;
import javafish.clients.opc.exception.UnableAddGroupException;
import javafish.clients.opc.exception.UnableAddItemException;
import javafish.clients.opc.exception.UnableRemoveGroupException;
import javafish.clients.opc.variant.Variant;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;



public class FXMLPrincipalController implements Initializable {
    /*
            componentes visuais do javafx, gerados pelo scene builder,
        os nomes dos compoentes graficos vc coloca na aba propriedade 'properties'
        no campo 'ID'
       
            para abrir o arquivo FXML primeiramente vc precisa do scene builder instalado,
        apos instacao basta da duplo clique no arquivo .fxml
    */
    @FXML
    private AnchorPane AnchorPane;
    @FXML
    private Tile tile_sp;
    @FXML
    private Tile tile_mv;
    @FXML
    private Tile tile_pv;
    @FXML
    private JFXTextField txt_opcServer;
    @FXML
    private JFXTextField txt_host;
    @FXML
    private JFXButton btn_connect;
    @FXML
    private Label lbl_serverStatus;
    @FXML
    private JFXTextField txt_tagSP;
    @FXML
    private JFXTextField txt_tagPV;
    @FXML
    private JFXTextField txt_tagMV;
    @FXML
    private JFXTextField txt_tagModo;
    @FXML
    private JFXComboBox<String> cmb_samples; //JFXComboBox<tipo_dado_inseriodo> 
    @FXML
    private JFXButton btn_iniciarLeitura;
    @FXML
    private LineChart grafico;
    @FXML
    private JFXButton btn_desconectar;
    @FXML
    private JFXButton btn_paraLeitura;
    @FXML
    private JFXButton btn_iniciaRele;
    @FXML
    private JFXButton btn_paraRele;
    @FXML
    private JFXTextField txt_eps;
    @FXML
    private JFXTextField txt_amp;
    @FXML
    private JFXTextField txt_tau;
    @FXML
    private JFXTextField txt_K;
    @FXML
    private JFXTextField txt_D;
    @FXML
    private JFXRadioButton rd_PI;
    @FXML
    private JFXRadioButton rd_PID;
    @FXML
    private JFXComboBox<String> cmb_metodos; //JFXComboBox<tipo_dado_inseriodo> 
    @FXML
    private JFXTextField txt_taui;
    @FXML
    private JFXTextField txt_Kp;
    @FXML
    private JFXTextField txt_tauD;
    @FXML
    private JFXButton btn_enviarSintonia;
    @FXML
    private Label lbl_kp;
    @FXML
    private Label lbl_ki;
    @FXML
    private Label lbl_kd;
    @FXML
    private JFXButton btn_modo;
    @FXML
    private ToggleGroup rdbGroupSintonia;
    @FXML
    private JFXTextField txt_SP;
    
    /* end componentes visuais do javafx */

    
    /*objetos aux*/
        
        /* objetos opc */
            public JOpc jopc; /*objeto opc */
            public OpcItem tagSP, tagPV, tagMV, tagModo, tagKp, tagKi, tagKd; /*objeto das tags opc */
            public OpcGroup group; /*objeto grupo opc */
            public static OpcGroup response; /*objeto para armazenar as leituras das tags opc */
        /* end objetos opc */
        
        /* variaveis valores interface*/
            public double SP, PV, MV, Modo,KP,KI,KD,erro=0,amp,eps,MVop; //eps = histerese, folga do rele
         /* end variaveis valores interface*/
            
        
            public PauseTransition wait; /*versao do Thread.sleep do javafx*/
        
        /*flags*/
            public boolean leitura; /*variavel para habilitar ou nao a leitura das tags*/
            public boolean flagRele = false; /*variavel para habilitar ou nao o rele*/
        /*flags*/
            
        private Thread graficThread; /*Thread para atualizar o grafico */
        
        /*objetos para o grafico*/
            private ObservableList<XYChart.Series<String, Double>> graficData; /*fonte dos dados do grafico*/
            public Series<String, Double> seriesSP = new Series<>(); /*dados do grafico, equivalente as penas no scada*/
            public Series<String, Double> seriesPV = new Series<>(); /*dados do grafico, equivalente as penas no scada*/
            public Series<String, Double> seriesMV = new Series<>(); /*dados do grafico, equivalente as penas no scada*/
        /*end objetos para o grafico*/
            
        /*variaveis para o rele*/    
            public ArrayList<Double> y = new ArrayList<Double>();//saidas
            public ArrayList<Double> u = new ArrayList<Double>();// entradas
            public ArrayList<Integer> periodo = new ArrayList<Integer>();
            int ciclos,cont,k;// variaveis de operacao para o rele
         /*end variaveis para o rele*/  
        
        /*fatores aux*/
            double fatorTags = 163.83; /*fatos para leitura e escrita das tags*/
            double fatorSintonia = 100.0;/*fatos para leitura e escrita das tags de sintonia*/
        /*fatores aux*/
            
    /*and objetos aux*/
    

    // ----------------------------- funcoes aux -------------------------------
    
    /* funcao para conectar ao servidor opc*/
    public void conectar() throws UnableAddGroupException, UnableAddItemException {
        JOpc.coInitialize();
        // instancia objecto do tipo Jopc
        jopc = new JOpc(txt_host.getText(), txt_opcServer.getText(), "opc");

        // crias tags
        tagMV = new OpcItem(txt_tagMV.getText(), true, "");
        tagPV = new OpcItem(txt_tagPV.getText(), true, "");
        tagSP = new OpcItem(txt_tagSP.getText(), true, "");
        tagModo = new OpcItem(txt_tagModo.getText(), true, "");
        tagKp = new OpcItem("[P_UNP]N7:16", true, "");
        tagKi = new OpcItem("[P_UNP]N7:17", true, "");
        tagKd = new OpcItem("[P_UNP]N7:18", true, "");

        // cria um grupo
        group = new OpcGroup("grupo1", true, 500, 0.0f);

        // add as tags ao grupo
        group.addItem(tagMV);
        group.addItem(tagPV);
        group.addItem(tagSP);
        group.addItem(tagModo);
        group.addItem(tagKp);
        group.addItem(tagKi);
        group.addItem(tagKd);
        
        // add o grupo ao OPC
        jopc.addGroup(group);

        // tenta conectar ao servidor opc
        try { //sucesso
            jopc.connect(); // conecta no server
            jopc.registerGroups(); // registra os grupos
            lbl_serverStatus.setText("Conectado!");
            classes.PushNoti.pushSuccess("Conectado ao servidor com sucesso", 2.0); //exibe um popup de sucesso
        } catch (ComponentNotFoundException | ConnectivityException e) { //falha
            classes.PushNoti.pushError("Erro ao tentar conectar ao Servidor OPC."
                    + " Verifique a conexao", 2);//exibe um popup de falha
        }
    }
    
    /* funcao para desconectar ao servidor opc*/
    public void desconectar() {
        try {
            jopc.unregisterGroups(); // desregistra os grupos
            PushNoti.pushSuccess("Desconectado com sucesso", 5); //exibe um popup de sucesso
        } catch (UnableRemoveGroupException ex) {
            Logger.getLogger(OPC.class.getName()).log(Level.SEVERE, null, ex);
            PushNoti.pushError("Erro ao tentar desconectar do servidor", 5);//exibe um popup de falha
        }
        JOpc.coUninitialize();// desinicialina o objeto opc
    }
    
    /* funcao para ler as tags do servidor  opc*/
    public void lerTags() throws ComponentNotFoundException, SynchReadException, InterruptedException {

        try {

            response = jopc.synchReadGroup(group); // retorna um vetor com as leituras do grupo

        } catch (ComponentNotFoundException | SynchReadException e) {
            //PushNoti.pushError("Erro na leitura das Tags", 1.5);
            Logger.getLogger(OPCReadFX.class.getName()).log(Level.SEVERE, null, e);
        }
        /* separa cada indice do response nas tags respectiva */
        MV = (Double.parseDouble(response.getItems().get(0).getValue().toString())/fatorTags);
        PV = (Double.parseDouble(response.getItems().get(1).getValue().toString())/fatorTags);
        SP = (Double.parseDouble(response.getItems().get(2).getValue().toString())/fatorTags);
        
        Modo = Double.parseDouble(response.getItems().get(3).getValue().toString());
        
        lbl_kp.setText(truncar(Double.parseDouble(response.getItems().get(4).getValue().toString()) / fatorSintonia).replace(",", "."));
        lbl_ki.setText(truncar(Double.parseDouble(response.getItems().get(5).getValue().toString()) / fatorSintonia).replace(",", "."));
        lbl_kd.setText(truncar(Double.parseDouble(response.getItems().get(6).getValue().toString()) / fatorSintonia).replace(",", "."));
        
        /* end separa cada indice do response nas tags respectiva */
        
        /*atualiza dos tiles*/
        tile_mv.setValue(MV);
        tile_pv.setValue(PV);
        tile_sp.setValue(SP);
        /*end atualiza dos tiles*/
        
        
         /*atualiza o botao. */
        if (Modo > 0) { // botao em manual
            btn_modo.setStyle("-fx-background-color:RED;");
            btn_modo.setText("Manual");
        } else {// botao em automativo
            btn_modo.setStyle("-fx-background-color:GREEN");
            btn_modo.setText("Auto");
        }
        
        
        if (flagRele) {//
            if (cont <= ciclos) {
                rele();// chama a funcao do metodo do rele
            } else {
                pararRele();// chama a funcao para parar o metodo do rele
                
                double amostragem = Double.parseDouble(cmb_samples.getValue()) / 1000;// pega o valor do tempo de amostragem
                ArrayList<Double> param = ModFOPDT(periodo, amp, eps, SP, y, u, amostragem); // calcula K,Tal,D

                txt_K.setText(truncar(Double.parseDouble(param.get(0).toString())).replaceAll(",", ".")); // exite o valor de K na interface
                txt_tau.setText(truncar(Double.parseDouble(param.get(1).toString())).replaceAll(",", ".")); // exite o valor de Tau na interface
                txt_D.setText(truncar(Double.parseDouble(param.get(2).toString())).replaceAll(",", ".")); // exite o valor de D na interface
                // replaceAll troca a "," por "."
            }
        }
    }

    /* funcao contem um Thread para fazer a leitura das tags em loop*/
    public void loopLeitura() {
        /*o jfx nao permite q vc faca alteracao nas view como bem entender, 
        eh necessario criar uma task para fazer alteracao na view e adiciona-la a uma Thread*/
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                            graficData.addAll(seriesMV, seriesPV, seriesSP);//adiciona as penas no grafico
                            if(leitura){
                                wait = new PauseTransition(Duration.millis(Double.parseDouble(cmb_samples.getValue().toString())));// versao do Thread.sleep para jfx
                                wait.setOnFinished((e) -> {
                                    try {
                                        lerTags();//chama a funcao para realizar a leitura das tags
                                        atualizaGrafico();//chama a funcao para realizar a atualizacao do grafico
                                    } catch (ComponentNotFoundException | SynchReadException ex) {
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(FXMLPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    wait.playFromStart();
                                });
                                wait.play();//inicia a atualizacao do grafico
                            }
                            else{
                                wait.stop();//para a atualizacao do grafico
                            }
                    }
                });
                return null;
            }
        };
        new Thread(task).start();
    }

    /* funcao para atualizar o grafico*/
    public void atualizaGrafico() {

        Calendar calendar = Calendar.getInstance(); //calendario para pegar a hora corrente do sistema
        String date = String.format("%d:%d:%d",
                calendar.get(Calendar.HOUR),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND)
        );
        
        seriesSP.getData().add(new XYChart.Data(date, SP)); // dados para o grafico    
        seriesPV.getData().add(new XYChart.Data(date, PV)); 
        seriesMV.getData().add(new XYChart.Data(date, MV));

        if (seriesSP.getData().size() > 15) { 
            /*quando a quantidade de dados no grafico for maior que 15,
            faz a remocao dos primeiros dados para nao conprimir as informacoes do grafico*/
            seriesSP.getData().remove(0);
            seriesPV.getData().remove(0);
            seriesMV.getData().remove(0);
        }
    }
    
    /* funcao para fazer a escrita no servidor opc*/
    private void escrever(OpcItem tag, double valor) {
         tag.setValue(new Variant(valor)); // seta o valor na tag
        try {
            jopc.synchWriteItem(group, tag); // escreve o valo na tag
        } catch (ComponentNotFoundException | SynchWriteException ex) {
            Logger.getLogger(FXMLPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /* funcoa para realizar o metodo se sintonia do rele*/
    public void rele(){
        erro = SP - PV; // calcula o erro
        
        /*faz o chaveamento do rele*/
        if (cont == 0) {                                
            if (erro >= 0) {
                escrever(tagMV,(MVop + amp)*fatorTags);
            }else{
                escrever(tagMV,(MVop - amp)*fatorTags);
            }
        }else if(erro >= eps){
            escrever(tagMV, (MVop + amp)*fatorTags);
        }else if(erro <= -eps){
            escrever(tagMV, (MVop - amp)*fatorTags);
        }       
        
        /*end faz o chaveamento do rele*/
        
        y.add(PV); //adiciona as saidas no vetor de saidas
        u.add(MV); //adiciona as entradas no vetor de entradas
        
        if (k > 1) {
           /* verifica se houve mudanca no sinal*/
           if(Math.round(u.get(k))!= Math.round(u.get(k-1))){
               cont++; //houve mudanca
               periodo.add(k);
           }
        }
        k++;
    }
    
    /*funcao para da um delay*/
    public void esperar(String tempo) throws InterruptedException{
        Long t = Long.parseLong(tempo);
        if (t > 0) {
            Thread.sleep(t);
        }
    }
    
    /* funcao truncar os valores para ficarem com 2 casas decimais*/
    public static String truncar(double valor){
        DecimalFormat df = new DecimalFormat("#0.00"); // formato do valor 
        return df.format(valor); // aplica o formato
    }
    
    /*funcoa setup do rele*/
    public void iniciarRele() throws InterruptedException{
        escrever(tagModo,1); //manual
        esperar("20"); //delay de 20ms
        k = 0;
        cont = 0;
        y.clear();// limpa o vetor de saidas
        u.clear(); // limpa o vetor de entradas
        MVop = MV; // pega a mv de operacao
        
        amp = Double.parseDouble(txt_amp.getText());  // pega o valor da amplitude do rele
        eps = Double.parseDouble(txt_eps.getText());  // pega o valor da histerese do rele
        
        flagRele = true; // inicia o rele
        
    }
    
    /*funcao para parar o rele*/
    public void pararRele() throws InterruptedException{
        escrever(tagModo, 0); // automatico
        esperar("20"); //delay de 20ms
        flagRele = false; //para leitura do rele
    }
    
    /*funcao cedida pelo professor jan  para retornar K,Tau,D */
    public static ArrayList<Double> ModFOPDT(ArrayList<Integer> periodos, double amp, double eps, double ref, ArrayList<Double> y, ArrayList<Double> u, double Tamostragem) {
        ArrayList<Double> ParFOPDT = new ArrayList<>();
        double Tu, RefAux, Au, Ad, a, Ku, fase, k, tau, teta;
        int aux1, aux2;
        ParFOPDT.clear();

        //calculo do periodo total
        Tu = ((periodos.get(5) - periodos.get(4)) * Tamostragem) + ((periodos.get(4) - periodos.get(3)) * Tamostragem);
        aux1 = periodos.get(2);
        aux2 = periodos.get(4);
        double yi1[] = new double[aux2 - aux1 + 2];
        double yi2[] = new double[aux2 - aux1 + 2];
        double ui1[] = new double[aux2 - aux1 + 2];
        double ui2[] = new double[aux2 - aux1 + 2];
        double ti1[] = new double[aux2 - aux1 + 2];
        double ti2[] = new double[aux2 - aux1 + 2];

        RefAux = ref;
        for (int t = aux1; t <= aux2 - 1; t++)//pico de positivo
        {
            if (y.get(t - 1) >= RefAux) {
                RefAux = y.get(t - 1);
            }
        }
        Au = RefAux;//guardar pico de subida
        RefAux = ref;

        for (int t = aux1; t <= aux2 - 1; t++)//pico negativo
        {
            if (y.get(t - 1) <= RefAux) {
                RefAux = y.get(t - 1);
            }
        }
        Ad = RefAux;

        a = (Math.abs(Au) - Math.abs(Ad)) / 2;//amplitude de sa�da
        //fun��o descritiva do rel�
        Ku = (4 * amp) / (Math.PI * a);
        fase = Math.asin((eps / a)) * -1;//calcular defasagem da histerese
        //-----<Inicio do Calculo Ganho est�tico - M�etodo da Integral>---------------- 
        int i = 0;
        yi1[i] = 0;
        ui1[i] = 0;
        ti2[i] = 0;
        for (int t = aux1; t <= aux2; t++) {//la�o para colher os dados de 1 periodos completo do teste
            yi1[i + 1] = y.get(t - 1);
            yi2[i] = y.get(t - 1);

            ui1[i + 1] = u.get(t - 1);
            ui2[i] = u.get(t - 1);

            ti1[i] = (i + 1) * Tamostragem;
            ti2[i + 1] = (i + 1) * Tamostragem;
            i = i + 1;
        }
        yi2[i] = 0;
        ui2[i] = 0;
        ti1[i] = 0;

        Matrix Yi1 = new Matrix(yi1, 1);
        Matrix Yi2 = new Matrix(yi2, 1);
        Matrix Ti1 = new Matrix(ti1, 1);
        Matrix Ti2 = new Matrix(ti2, 1);

        Yi1 = Yi1.plusEquals(Yi2);
        Ti1 = Ti1.minusEquals(Ti2);
        Yi1 = Yi1.arrayTimes(Ti1).times(0.5);

        double A1 = 0;
        for (int j = 1; j < Yi1.getColumnDimension() - 1; j++) {
            A1 = A1 + Yi1.get(0, j);
        }
        Matrix Ui1 = new Matrix(ui1, 1);
        Matrix Ui2 = new Matrix(ui2, 1);

        Ui1 = Ui1.plusEquals(Ui2);
        Ui1 = Ui1.arrayTimes(Ti1).times(0.5);
        //Ui1 = Ui1.times(0.5);

        double A2 = 0;
        for (int j = 1; j < Ui1.getColumnDimension() - 1; j++) {
            A2 = A2 + Ui1.get(0, j);
        }

        k = A1 / A2;//ganho est�tico
        double delta;
        if (Ku * k < 1) {
            delta = 1;
        } else {
            delta = Math.pow((Ku * k), 2) - 1;
        }

        //----<Fim do calculo ganho est�tico da planta>-------------------------------
        tau = (Tu / (2 * Math.PI)) * Math.sqrt(delta);
        teta = (Tu / (2 * Math.PI)) * (Math.PI - Math.atan((2 * Math.PI * tau) / Tu) + fase);

        ParFOPDT.add(0, k);
        ParFOPDT.add(1, tau);
        ParFOPDT.add(2, teta);

        return ParFOPDT;
    }
    
    /*funcao para fazer o calculo da sintonia de ZN*/
    public void ZieglerNichols(double K, double tau, double teta, boolean PID) {
        double p, tauI, Ki;

        if (!PID) { //cacula sintonia PI
            p = 0.9 * (tau / (K * teta));
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = (3.33 * teta);
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            txt_tauD.setText("0");
        } else { //cacula sintonia PI
            double Kd, tauD;

            p = 1.2 * (tau / (K * teta));
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = (2 * teta);
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            tauD = (0.5 * teta) / 60;
            Kd = p * tauD;
            txt_tauD.setText(truncar(Kd*fatorSintonia));
        }
    }

    /*funcao para fazer o calculo da sintonia de CHR*/
    public void CHR(double K, double tau, double teta, boolean PID) {
        double p, tauI, Ki;

        if (!PID) { //cacula sintonia PI
            p = 0.6 * (tau / (K * teta));
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = (4 * teta);
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            txt_tauD.setText("0");
        } else {//cacula sintonia PID
            double Kd, tauD;

            p = 0.95 * (tau / (K * teta));
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = (2.375 * teta);
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            tauD = (0.421 * teta) / 60;
            Kd = p * tauD;
            txt_tauD.setText(truncar(Kd*fatorSintonia));
        }
    }

    /*funcao para fazer o calculo da sintonia IAE*/
    public void IAE(double K, double tau, double teta, boolean PID) {
        double p, tauI, Ki;

        if (!PID) {//cacula sintonia PI
            p = (0.758 / K) * Math.pow((tau / teta), 0.861);
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = (tau / (1.02 - 0.323 * (teta / tau)));
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            txt_tauD.setText("0");
        } else {//cacula sintonia PID
            double Kd, tauD;

            p = (0.1086 / K) * Math.pow((tau / teta), 0.869);
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = tau / (0.740 - 0.130 * (teta / tau));
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            tauD = (0.348 * tau * Math.pow((teta / tau), 0.914)) / 60;
            Kd = p * tauD;
            txt_tauD.setText(truncar(Kd * fatorSintonia));
        }
    }

    /*funcao para fazer o calculo da sintonia ITAE*/
    public void ITAE(double K, double tau, double teta, boolean PID) {
        double p, tauI, Ki;

        if (!PID) { //cacula sintonia PI
            p = (0.586 / K) * Math.pow((tau / teta), 0.916);
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = tau / (1.03 - 0.165 * (teta / tau));
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            txt_tauD.setText("0");
        } else {//cacula sintonia PID
            double Kd, tauD;

            p = (0.965 / K) * Math.pow((tau / teta), 0.850);
            txt_Kp.setText(truncar(p * fatorSintonia));
            tauI = tau / (0.796 - 0.147 * (teta / tau));
            Ki = (p / tauI) / 60;
            txt_taui.setText(truncar(Ki * fatorSintonia));
            tauD = (0.308 * tau * Math.pow((teta / tau), 0.929)) / 60;
            Kd = p * tauD;
            txt_tauD.setText(truncar(Kd * fatorSintonia));
        }
    }

    /*funcao para fazer o calculo da sintonia PI de acordo com o metodo selecionado no combobox*/
    public void PI() {

        double p, K, tau, teta, tauI, Ki;

        K = Double.parseDouble(txt_K.getText()); //pega o valor de K
        tau = Double.parseDouble(txt_tau.getText()); //pega o valor de Tau
        teta = Double.parseDouble(txt_D.getText()); //pega o valor de D

        if (null != cmb_metodos.getValue()) {
            switch (cmb_metodos.getValue().toString()) {
                case "Ziegler-Nichol":
                    ZieglerNichols(K, tau, teta, false);
                    break;
                case "CHR":
                    CHR(K, tau, teta, false);
                    break;
                case "IAE":
                    IAE(K, tau, teta, false);
                    break;
                case "ITAE":
                    ITAE(K, tau, teta, false);
                    break;
                default:
                    break;
            }
        }
    }

    /*funcao para fazer o calculo da sintonia PID de acordo com o metodo selecionado no combobox*/
    public void PID() {
        double p, K, tau, teta, tauI, tauD, Kp, Ki, Kd;

        K = Double.parseDouble(txt_K.getText()); //pega o valor de K
        tau = Double.parseDouble(txt_tau.getText()); //pega o valor de Tau
        teta = Double.parseDouble(txt_D.getText()); //pega o valor de D

        if (null != cmb_metodos.getValue()) {
            switch (cmb_metodos.getValue().toString()) {
                case "Ziegler-Nichol":
                    ZieglerNichols(K, tau, teta, true);
                    break;
                case "CHR":
                    CHR(K, tau, teta, true);
                    break;
                case "IAE":
                    IAE(K, tau, teta, true);
                    break;
                case "ITAE":
                    ITAE(K, tau, teta, true);
                    break;
                default:
                    break;
            }
        }

    }
    
    /*funcao para fazer a checagem de qual controlador foi selecionado*/
    public void sintonia(){
        if (rd_PI.isSelected()) { // selecionado radioButton PI
            PI(); // chama funcao PI
        } else if (rd_PID.isSelected()) { // selecionado radioButton PID
            PID(); // chama funcao PID
        }
    }
    
    /*funcao para criar um alerta quando eh feita alguma escrita no OPC*/
    public void alert(String conteudo,boolean sintonia) throws InterruptedException{
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION); // cria uma caixa de alerta do tipo confirmacao
        
        alert.setTitle("Confirmacao"); //titulo
        alert.setHeaderText("Alerta de confimacao"); // cabecalho(subtitulo)
        alert.setContentText(conteudo); //contudo do aleta
        
        Optional<ButtonType> result = alert.showAndWait(); // adiciona buttons no alerta
        if (sintonia) { // enviar sintonia?
            if (result.get() == ButtonType.OK) { // clicado no botao ok?
                escrever(tagKp, Double.parseDouble(txt_Kp.getText().replace(',', '.')));
                esperar("20");
                escrever(tagKi, Double.parseDouble(txt_taui.getText().replace(',', '.')));
                esperar("20");
                escrever(tagKd, Double.parseDouble(txt_tauD.getText().replace(',', '.')));
                esperar("20");
            }else{
                alert.close(); // se clicado no botao cancelar, fecha o alerta
            }
        }else{ // envia SP
            if (result.get() == ButtonType.OK) { // se clicado no botao ok, envia SP
                double sp = (Double.parseDouble(txt_SP.getText().replace(',', '.'))*fatorTags);
                escrever(tagSP, sp);
                esperar("20");
            }else{
                alert.close();// se clicado no botao cancelar, fecha o alerta
            }
        }
    }
    
    // --------------------------- end funcoes aux -----------------------------
    
    
     // ---------------------------  funcoes javafx  -----------------------------
    
    /*
            as funcao a baixo foram geradas pelo, gerados pelo scene builder.
        o programador do precisar colocar o nome da funcao no campo 'on action' na aba code,
        salva o arquivo, no netbeans vc clica com o botao direito no arquivo FXML e clica em controlador make
        ai o netbeans importa as funcoes e compnentes graficos adicionados no scene builder
    */
    
    /*chamada qunado a view eh chamada, neste caso quando o programa abre*/
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        seriesSP.setName(String.format("SP")); //legenda
        seriesPV.setName(String.format("PV"));
        seriesMV.setName(String.format("MV"));
        
        
        graficData = FXCollections.observableArrayList();// observableArrayList sao array de atualizacao instatanea, seria tipo um array listen
        grafico.setCreateSymbols(false); // tira os pontos de conexao do grafico
        grafico.setData(graficData); // adicionar a fonte de dados no grafico
        
        /*coloca os periodos de amostraem no combobox*/
        cmb_samples.getItems().add("100");
        cmb_samples.getItems().add("200");
        cmb_samples.getItems().add("300");
        cmb_samples.getItems().add("500");
        cmb_samples.getItems().add("1000");
        cmb_samples.getItems().add("1500");
        cmb_samples.getItems().add("2000");
        cmb_samples.getItems().add("2500");
        cmb_samples.getItems().add("3000");
        
        /*coloca os metodos de sintonia no combobox*/
        cmb_metodos.getItems().add("Ziegler-Nichol");
        cmb_metodos.getItems().add("CHR");
        cmb_metodos.getItems().add("IAE");
        cmb_metodos.getItems().add("ITAE");

        
        txt_opcServer.setText("RSLinx OPC Server");// deixa gravado o ID do server OPC
        txt_host.setText("localhost"); // deixa gravado o ip do server opc
        txt_tagSP.setText("[P_UNP]N7:19"); // deixa gravado o enderedo da tag SP
        txt_tagPV.setText("[P_UNP]N7:59");// deixa gravado o enderedo da tag PV
        txt_tagMV.setText("[P_UNP]N7:20");// deixa gravado o enderedo da tag MV
        txt_tagModo.setText("[P_UNP]B19:0/2");// deixa gravado o enderedo da tag MODO
        
        btn_desconectar.setDisable(true); //desabilita o botao desconecta
        btn_paraLeitura.setDisable(true); //desabilita o botao para leitura do grafico
        btn_iniciarLeitura.setDisable(true); //desabilita o botao iniciar leitura do grafico
        
        ciclos = 6;// ciclos do rele
    }
    /*funcao evento de click do buttao conectar*/
    @FXML
    private void conectar(ActionEvent event) throws ComponentNotFoundException, SynchReadException, UnableAddGroupException, UnableAddItemException {
        conectar();// chama a funcao para conectar ao servidor
        
        btn_connect.setDisable(true); //desabilita o botao conectar 
        btn_desconectar.setDisable(false); //abilita o botao desconectar 
        btn_iniciarLeitura.setDisable(false); //abilita o botao iniciar leitura 
    }
    
    /*funcao evento de click do buttao conectar*/
    @FXML
    private void desconectar(ActionEvent event){
        desconectar(); // chama a funcao desconectar 
        btn_connect.setDisable(false); // abilita o botao conectar 
        btn_desconectar.setDisable(true); //desabilita o botao desconectar
        btn_iniciarLeitura.setDisable(true); //desabilita o botao iniciarleitura
    }
    
    /*funcao evento de click do buttao iniciar leitura*/
    @FXML
    private void iniciaLeitura(ActionEvent event) {
        leitura = true; // abilita leitura em loop
        loopLeitura(); // chama a funcao de leitura em loop
        btn_iniciarLeitura.setDisable(true); // desabilita o botao iniciar leitura
        btn_paraLeitura.setDisable(false);// abilita o botao para leitura
        
    }
    
    /*funcao evento de click do buttao iniciar leitura*/
    @FXML
    private void paraLeitura(ActionEvent event) {
        leitura = false; //desabilita leitura em loop
        loopLeitura(); //chama a funcao de leitura em loop
        btn_paraLeitura.setDisable(true); //desabilita o botao iniciar leitura
        btn_iniciarLeitura.setDisable(false); //abilita o botao para leitura
    }
    
    /*funcao evento de click do buttao iniciar rele*/
    @FXML
    void iniciaRele(ActionEvent event) throws InterruptedException {
         iniciarRele();// chama funcao pra iniciar o rele
    }
    
    /*funcao evento de click do buttao parar rele*/
    @FXML
    void paraRele(ActionEvent event) throws InterruptedException {
        pararRele();// chama funcao pra parar o rele
    }
    
    /*funcao evento de click do buttao enviar sintonia*/
    @FXML
    void enviarSintonia(ActionEvent event) throws InterruptedException {
        /*antes de enviar eh exibido um alerta perguntando se realmente deseja enviar a sintonia.
        o aparametro booleano 'sintonia' quando true indica que desenha enviar kp,ki,kd*/
        alert("Deseja reaelmente alterar a sintonia ?", true);
    }
    
    /*funcao evento de click do buttao modo*/
    @FXML
    private void statusPlanta(ActionEvent event) {
        /*botao para colocar a planta em manual ou automatica*/
        if (btn_modo.getText() == "Auto") {
            escrever(tagModo, 1);//colocar em manual
            btn_modo.setText("Manual");
            btn_modo.setStyle("-fx-background-color:RED");
        } else {
            escrever(tagModo, 0);//Colocar em Automático
            btn_modo.setText("Auto");
            btn_modo.setStyle("-fx-background-color:GREEN");
        }
        
    }
    
    /*funcao evento de click do combobox dos metodos*/
    private void sintonia(ActionEvent event){
        sintonia();//chama a funcao para realizar o calculo da sintonia selecionadad do metodo selecionado
    }
    
     /*funcao evento de click do buttao enviar SP*/
    @FXML
    private void enviarSP(ActionEvent event) throws InterruptedException {
        /*antes de enviar eh exibido um alerta perguntando se realmente deseja enviar o SP.
        o aparametro booleano 'sintonia' quando false indica que desenha enviar sp*/
        alert("Deseja reaelmente alterar o SetPoint ?", false);
    }
    
    // ---------------------------  end funcoes javafx  -----------------------------

    


}
