/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafish.clients.opc.JOpc;
import javafish.clients.opc.component.OpcGroup;
import javafish.clients.opc.component.OpcItem;
import javafish.clients.opc.exception.ComponentNotFoundException;
import javafish.clients.opc.exception.ConnectivityException;
import javafish.clients.opc.exception.SynchReadException;
import javafish.clients.opc.exception.UnableAddGroupException;
import javafish.clients.opc.exception.UnableAddItemException;
import javafish.clients.opc.exception.UnableRemoveGroupException;

/**
 *
 * @author pedro
 */
public class OPC {

    public OPC() {
        
    }
    
    
    public JOpc jopc;
    public OpcGroup responseGroup,group;
    private OpcItem tagSP, tagPV, tagMV, tagModo, tagKp, tagKi, tagKd;
    
       
    
    public void conectar(String host,String serverID,String TagMV,String TagPV,String TagSP,String TagModo) throws UnableAddGroupException, UnableAddItemException {
       
        JOpc.coInitialize();
        // instancia objecto do tipo Jopc
        jopc = new JOpc(host,serverID,"opc");
        
        // crias tags
        tagMV    = new OpcItem(TagMV,true,"");
        tagPV    = new OpcItem(TagPV,true,"");
        tagSP    = new OpcItem(TagSP,true,"");
        tagModo  = new OpcItem(TagModo,true,"");
        
        // cria um grupo
        group = new OpcGroup("g",true,500,0.0f);
        
        // add as tags ao grupo
        group.addItem(tagMV);
        group.addItem(tagPV);
        group.addItem(tagSP);
        group.addItem(tagModo);
        
        jopc.addGroup(group);
        
        // tenta conectar ao servidor opc
        try { //sucesso
            jopc.connect();
            jopc.registerGroups();
            classes.PushNoti.pushSuccess("Conectado ao servidor com sucesso", 2.0);           
        } catch (ComponentNotFoundException | ConnectivityException e) { //falha
            classes.PushNoti.pushError("Erro ao tentar conectar ao Servidor OPC."
                    + " Verifique a conexao", 2);   
        }
        
    }
    
    public void desconectar() {
        try {
            jopc.unregisterGroups();
            PushNoti.pushSuccess("Desconectado com sucesso", 5);
        } catch (UnableRemoveGroupException ex) {
            Logger.getLogger(OPC.class.getName()).log(Level.SEVERE, null, ex);
            PushNoti.pushError("Erro ao tentar desconectar do servidor", 5);
        }
        JOpc.coUninitialize();
    }
    
    public synchronized OpcGroup lerTags(){
        
        try {
            responseGroup = jopc.synchReadGroup(group);
            //PushNoti.pushSuccess("Leitura realizada com sucesso", );
            return responseGroup;
        } catch (ComponentNotFoundException | SynchReadException e) {
            //PushNoti.pushError("Erro na leitura das Tags", 1.5);
            return null;
        }   
    }
    
    public double valueMV() throws SynchReadException{
        OpcGroup grpresp;
        try {
            grpresp = jopc.synchReadGroup(group);
            Double MV = Double.parseDouble((grpresp.getItems().get(0).getValue().toString()));
            //PushNoti.pushSuccess("Leitura realizada com sucesso", 1.5);
            return MV;
        } catch (ComponentNotFoundException ex) {
            Logger.getLogger(OPC.class.getName()).log(Level.SEVERE, null, ex);
            PushNoti.pushError("Erro na leitura das Tags", 1.5);
            return 9999.99;
        }    
    }
    
    public double valuePV() throws SynchReadException{
        OpcGroup grpresp;
        try {
            grpresp = jopc.synchReadGroup(group);
            Double PV = Double.parseDouble((grpresp.getItems().get(1).getValue().toString()));
            //PushNoti.pushSuccess("Leitura realizada com sucesso", 1.5);
            return PV;
        } catch (ComponentNotFoundException ex) {
            Logger.getLogger(OPC.class.getName()).log(Level.SEVERE, null, ex);
            PushNoti.pushError("Erro na leitura das Tags", 1.5);
            return 9999.99;
        }    
    }
    
    public double valueSP() throws SynchReadException{
        OpcGroup grpresp;
        try {
            grpresp = jopc.synchReadGroup(group);
            Double SP = Double.parseDouble((grpresp.getItems().get(2).getValue().toString()));
            //PushNoti.pushSuccess("Leitura realizada com sucesso", 1.5);
            return SP;
        } catch (ComponentNotFoundException ex) {
            Logger.getLogger(OPC.class.getName()).log(Level.SEVERE, null, ex);
            PushNoti.pushError("Erro na leitura das Tags", 1.5);
            return 9999.99;
        }    
    }
    
    public double valueModo() throws SynchReadException{
        OpcGroup grpresp;
        try {
            grpresp = jopc.synchReadGroup(group);
            Double modo = Double.parseDouble((grpresp.getItems().get(3).getValue().toString()));
            //PushNoti.pushSuccess("Leitura realizada com sucesso", 1.5);
            return modo;
        } catch (ComponentNotFoundException ex) {
            Logger.getLogger(OPC.class.getName()).log(Level.SEVERE, null, ex);
            PushNoti.pushError("Erro na leitura das Tags", 1.5);
            return 9999.99;
        }    
    }
    
    
}
