/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import javafx.geometry.Pos;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;


/**
 *
 * @author pedro
 */
public class PushNoti {

    public static void pushInfo(String Message,double delay){
        TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.INFORMATION);
            tray.setTitle("INFO");
            tray.setMessage(Message);
            tray.setAnimationType(AnimationType.POPUP);
            
        tray.showAndDismiss(Duration.millis(1500));
    }
    
    public static void pushError(String Message,double delay){
        TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.ERROR);
            tray.setTitle("Erro");
            tray.setMessage(Message);
            tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.millis(1500));
    }
    
    public static void pushWarning(String Message,double delay){
        TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.WARNING);
            tray.setTitle("Aviso");
            tray.setMessage(Message);
            tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.millis(1500));
    }
    
    public static void pushSuccess(String Message,double delay){
        TrayNotification tray = new TrayNotification();
            tray.setNotificationType(NotificationType.SUCCESS);
            tray.setTitle("Sucesso");
            tray.setMessage(Message);
            tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.millis(1500));
    }
    
}
